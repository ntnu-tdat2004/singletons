# Thread-safe singleton examples

## Prerequisites
* Linux or MacOS
* The C++ IDE [juCi++](https://github.com/cppit/jucipp)

## Compile and run
```sh
git clone https://gitlab.com/ntnu-tdat2004/singletons
juci singletons
```

Choose Compile and Run in the Project menu.
