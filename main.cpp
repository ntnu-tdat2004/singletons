#include <iostream>
#include <memory>

using namespace std;

/// Regular singleton
class Singleton {
  // Constructor is private, the static get() method must be used to get/create the instance
  Singleton() {
    cout << "Singleton constructor called" << endl;
  }

public:
  static Singleton &get() {
    static Singleton instance;
    return instance;
  }
};

/// Singleton where its instance is destroyed when it is no longer in use
class ReferenceCountedSingleton {
  // Constructor is private, the static get() method must be used to get/create the instance
  ReferenceCountedSingleton() {
    cout << "ReferenceCountedSingleton constructor called" << endl;
  }

public:
  /// If an instance is already in use, return cached instance, otherwise return a new instance.
  static shared_ptr<ReferenceCountedSingleton> get() {
    static weak_ptr<ReferenceCountedSingleton> cache;
    static mutex cache_mutex;
    lock_guard<mutex> lock(cache_mutex);
    auto instance = cache.lock();
    if (!instance)
      cache = instance = shared_ptr<ReferenceCountedSingleton>(new ReferenceCountedSingleton());
    return instance;
  }
};

int main() {
  {
    auto s1 = Singleton::get(); // An instance of Singleton is created and returned
    auto s2 = Singleton::get(); // The already created instance is returned
  }
  auto s3 = Singleton::get(); // The already created instance is returned


  {
    auto rcs1 = ReferenceCountedSingleton::get(); // Instance of ReferenceCountedSingleton is created
    auto rcs2 = ReferenceCountedSingleton::get(); // Already created instance is returned
  }                                               // ReferenceCountedSingleton's instance is destroyed
  auto rcs3 = ReferenceCountedSingleton::get();   // A new instance of ReferenceCountedSingleton is created
}
